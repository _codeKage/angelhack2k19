export class User {
  id: number;
  email: string;
  password: string;
  name: string;
  phone_number: string;
}
