import {CarBrandModel} from "./car-brand.model";
import {CarFuelModel} from "./car-fuel.model";
import {VehicleStateModel} from "./vehicle-state.model";

export class FleetMgrVehiclesModel {
  id: number;
  carbrandmodelid: number;
  carbrandmodel: CarBrandModel = null;
  chassisnumber: string;
  enginenumber: string;
  is_aircon: boolean;
  is_manual: boolean;
  vehiclestatus: number;
  $$vehicleStatus: VehicleStateModel = null;
  fueltypeid: number;
  fueltype: CarFuelModel = null;
  passengercapacity: number;
  rentalpricedaily: number;
  rentalpricehourly: number;
  vehiclepicurl: string;
}
