import {User} from "./user.model";

export class FleetManagerModel {
  id: number;
  userid: number;
  user: User = null;
  orgname: string;
  orgownername: string;
  address: string;
  contactnumber: string;
  contactemail: string;
  contactweb: string;
}
