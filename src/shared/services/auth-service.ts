import { Injectable } from "@angular/core";
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {apiEndpoint} from "../api-endpoint";
import {Storage} from "@ionic/Storage";
@Injectable()

export class AuthService {
  token: any;
  constructor(private http: HttpClient, private storage: Storage) {
    this.storage.get('token').then((token) => {
      this.token = JSON.parse(token);
    }).catch(() => {

    });
  }

  login(username: string, password: string): Observable<any> {
    return this.http.post(apiEndpoint+"auth/login/", {
      email: username,
      password: password
    });
  }

  logout(token: any): Observable<any> {
    return this.http.post(apiEndpoint+"auth/logout/", {}, {
      headers: {
        'Authorization': `Bearer ${token.access_token}`
      }
    });
  }

  getToken(): any {
    return this.token.access_token;
  }
}
