import { Component, ViewChild } from '@angular/core';
import {AlertController, Nav, Platform} from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { HomePage } from '../pages/home/home';
import {LoginPage} from "../pages/login/login";

import {Storage} from "@ionic/Storage";
import {AuthService} from "../shared/services/auth-service";
import {LocalNotifications} from "@ionic-native/local-notifications";
import {HttpClient} from "@angular/common/http";
import {apiEndpoint} from "../shared/api-endpoint";
import {PaymentPage} from "../pages/payment/payment";

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;

  rootPage: any = LoginPage;

  pages: Array<{title: string, component: any}>;

  notified: boolean;

  token: any;

  constructor(public platform: Platform, public statusBar: StatusBar, public splashScreen: SplashScreen, private alertCtrl: AlertController, private storage: Storage, private authService: AuthService, private localNotification: LocalNotifications, private http: HttpClient) {

    this.storage.get('token').then((token) => {
      this.token = token;
      this.rootPage = token === null ? LoginPage : HomePage;
    }).catch(() => {
      this.rootPage = LoginPage;
    });
      this.getNotifications();

    this.initializeApp();
    // used for an example of ngFor and navigation
    this.pages = [
      { title: 'Search', component: HomePage },
      { title: 'Payment', component: PaymentPage },
      { title: 'Logout', component: null }
    ];

  }

  initializeApp() {
    this.platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }

  openPage(page) {
    if(page.component === null) {
      this.alertCtrl.create({
        title: 'Message',
        subTitle: 'Do you really want to logout?',
        buttons: [{
          text: 'No',
          role: 'cancel'
        }, {
          text: 'Yes',
          handler: () => {
            this.storage.get('token').then((token) => {
              this.authService.logout(JSON.parse(token)).subscribe(() => {
                this.storage.remove('token').then(() => this.nav.setRoot(LoginPage));
              }, () => {
                this.storage.remove('token').then(() => this.nav.setRoot(LoginPage));
              });
            });
          }
        }]
      }).present();
      return;
    }
    this.nav.setRoot(page.component);
  }

  getNotifications(): void {
        setInterval(() => {
          console.log('here');
          if (this.rootPage !== LoginPage) {
            if (!this.notified) {
              this.http.get(apiEndpoint + "check/mobile", {
                headers: {
                  Authorization: `Bearer ${this.authService.getToken()}`
                }
              }).subscribe((response: any) => {
                if (response.status) {
                  this.localNotification.schedule({
                    title: 'Your request has been approved'
                  });
                  this.notified = true;
                }
              })
            }
          }
        }, 6000);
  }

}
