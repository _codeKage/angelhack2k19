import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { ListPage } from '../pages/list/list';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import {HttpClientModule} from "@angular/common/http";
import {LoginPage} from "../pages/login/login";
import {AuthService} from "../shared/services/auth-service";
import {IonicStorageModule} from "@ionic/Storage";
import {CarListPage} from "../pages/car-list/car-list";
import {CarInfoPage} from "../pages/car-info/car-info";
import {LocalNotifications} from "@ionic-native/local-notifications";
import {PaymentPage} from "../pages/payment/payment";


@NgModule({
  declarations: [
    MyApp,
    HomePage,
    ListPage,
    LoginPage,
    CarListPage,
    CarInfoPage,
    PaymentPage
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    IonicModule.forRoot(MyApp),
    IonicStorageModule.forRoot(),
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    ListPage,
    LoginPage,
    CarListPage,
    CarInfoPage,
    PaymentPage
  ],
  providers: [
    StatusBar,
    LocalNotifications,
    SplashScreen,
    AuthService,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}
