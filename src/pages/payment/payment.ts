import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {HttpClient} from "@angular/common/http";
import {AuthService} from "../../shared/services/auth-service";
import {apiEndpoint} from "../../shared/api-endpoint";

/**
 * Generated class for the PaymentPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-payment',
  templateUrl: 'payment.html',
})
export class PaymentPage {

  payments: any[] = [];

  constructor(public navCtrl: NavController, public navParams: NavParams, private http: HttpClient, private authService: AuthService) {
  }

  ionViewDidLoad() {
    this.initializePayments();
  }

  initializePayments(): void {
    this.http.get(apiEndpoint+"getAll/payments", {
      headers: {
        Authorization : `bearer ${ this.authService.getToken() }`
      }
    }).subscribe((payments: any[]) => {
      this.payments = payments;
      console.log(payments)
    });
  }

  payment(payment: any): void {
    this.http.post(apiEndpoint+`payment/module/${ payment.id }`, {}, {
      headers: {
        Authorization : `bearer ${ this.authService.getToken() }`
      }
    }).subscribe(() => {
      payment.paid = true;
      this.initializePayments();
    });
  }

}
