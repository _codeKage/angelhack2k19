import { Component } from '@angular/core';
import {AlertController, IonicPage, NavController, NavParams} from 'ionic-angular';
import {HttpClient} from "@angular/common/http";
import {apiEndpoint} from "../../shared/api-endpoint";
import {AuthService} from "../../shared/services/auth-service";
import {HomePage} from "../home/home";

@IonicPage()
@Component({
  selector: 'page-car-info',
  templateUrl: 'car-info.html',
})
export class CarInfoPage {

  car: any;
  pickupdate: string;
  returndate: string;
  zipcode: string;
  numberOfPassenger: string;
  hasReservation: boolean;

  constructor(public navCtrl: NavController, public navParams: NavParams, private http: HttpClient, private authService: AuthService, private alertController: AlertController) {
    this.car = this.navParams.get('car');
    this.pickupdate = this.navParams.get('pickupdate');
    this.returndate = this.navParams.get('returndate');
    this.numberOfPassenger = this.navParams.get('numberOfPassengers');
    this.zipcode = this.navParams.get('zipcode');
    this.http.get(apiEndpoint+"check/mobile/", {
      headers: {
        Authorization: `Bearer ${ this.authService.getToken() }`
      }
    }).subscribe((res: any) => {
      this.hasReservation = res.status;
    })
  }

  ionViewDidLoad() {
  }

  reserve(): void {
    this.http.post(apiEndpoint+"car/request/", {
      pickuploc: '(12, 12)',
      anotherloc: '(12, 12)',
      is_withchauffeur: false,
      is_returnanotherloc: false,
      startdate: this.pickupdate,
      enddate: this.returndate,
      passengercount: this.numberOfPassenger,
      preferredvehicle: this.car.id
    }, {
      headers: {
        Authorization: `Bearer ${ this.authService.getToken() }`
      }
    }).subscribe((resp) => {
      this.alertController.create({
        subTitle: 'Successfully Requested, Waiting for approval',
        buttons: ['ok']
      }).present();
      this.hasReservation = true;
      this.navCtrl.setRoot(HomePage);
    })
  }


}
