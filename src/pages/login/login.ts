import { Component } from '@angular/core';
import {AlertController, IonicPage, NavController, NavParams} from 'ionic-angular';
import {AuthService} from "../../shared/services/auth-service";
import {HomePage} from "../home/home";
import {Storage} from "@ionic/Storage";

/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {

  username: string;
  password: string;

  constructor(public navCtrl: NavController, public navParams: NavParams, private authService: AuthService, private alertCtrl: AlertController, private storage: Storage) {
  }

  ionViewDidLoad(): void {
  }

  login(): void {
    this.authService.login(this.username, this.password).subscribe((response) => {
      this.storage.set('token', JSON.stringify(response));
      this.navCtrl.setRoot(HomePage);
    }, (err) => {
      this.alertCtrl.create({
        message: JSON.stringify(err),
        buttons: ['ok']
      }).present();
    });
  }

}
