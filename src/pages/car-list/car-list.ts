import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {HttpClient} from "@angular/common/http";
import {apiEndpoint} from "../../shared/api-endpoint";
import {CarInfoPage} from "../car-info/car-info";
import * as dateFns from 'date-fns';

/**
 * Generated class for the CarListPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-car-list',
  templateUrl: 'car-list.html',
})
export class CarListPage {

  cars: any[] = [];
  numberOfDays: any;

  constructor(public navCtrl: NavController, public navParams: NavParams, private http: HttpClient) {
  }

  ionViewDidLoad() {
    this.http.get(apiEndpoint+"cars/all/").subscribe((cars: any[]) => {
      this.cars = cars;
    });

    this.numberOfDays = dateFns.differenceInCalendarDays(this.navParams.get('returndate'), this.navParams.get('pickupdate')) + 1;
  }

  openCarInfo(carInfo): void {
    this.navCtrl.push(CarInfoPage, {
      car: carInfo,
      pickupdate: this.navParams.get('pickupdate'),
      returndate: this.navParams.get('returndate'),
      zipcode: this.navParams.get('zipcode'),
      numberOfPassengers: this.navParams.get('numberOfPassengers')
    });
  }

}
