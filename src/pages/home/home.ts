import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import {CarListPage} from "../car-list/car-list";
import * as dateFns from 'date-fns';
import {Storage} from "@ionic/Storage";


@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  constructor(public navCtrl: NavController, private storage: Storage) {  }

  pickupdate: string;
  returndate: string;
  zipcode: string;
  numberOfPassengers: number;

  ionViewDidLoad(): void {
    this.storage.get('token').then((token) => {
      console.log(JSON.parse(token));
    })
  }

  openCarList(): void {
    this.createDates();
    console.log(this.numberOfPassengers);
    this.navCtrl.push(CarListPage, {
      pickupdate: this.pickupdate,
      returndate: this.returndate,
      zipcode: this.zipcode,
      numberOfPassengers: this.numberOfPassengers
    });
  }

  createDates(): any {
    this.pickupdate = dateFns.format(this.pickupdate,'YYYY-MM-DD');
    this.returndate = dateFns.format(this.returndate, "YYYY-MM-DD");
  }



}
